<!DOCTYPE>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Chat</title>
<link href="web/css/style_index.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="container">
<!-- bloc Container-->
  <div id="entete">
        <p id="entete_text">Bienvenue sur le <strong>chat</strong></p>
  </div>
 
  <div id="content">
		<h1>Connexion</h1>
		<?php if (isset($error_login)): ?>
			<p class="error"><?= $error_login; ?>
		<?php endif ?>
		<form method="post">
			Pseudo* : <input type="text" name="username" required="true">
			Mot de passe* <input type="password" name="passwd" required="true">
			<input type="submit" name="login" value="Connexion">
		</form>


		<h2>S'inscrire</h2>
		<?php if (isset($error_subscribe)): ?>
			<p class="error"><?= $error_subscribe; ?>
		<?php endif ?>
		<form method="post">
			<label>Pseudo* :</label>
			<input type="text" name="username" required="true"><br>
			<label>Mot de passe* :</label><input type="password" name="passwd" required="true"><br>
			<label>Mot de passe* :</label><input type="password" name="passwd2" required="true"><br>
			<input type="submit" name="subscribe" value="Connexion">
		</form>
<!-- end Blocs-->  
  </div>

<!-- End bloc Container-->
</div>
</body>
</html>