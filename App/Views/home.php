<!DOCTYPE>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Chat</title>
<link href="web/css/style_index.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="container">
<!-- bloc Container-->
  <div id="entete">
        <p id="entete_text">Bienvenue sur le <strong>chat</strong></p>
  </div>
  <div id="menu"></div>
  <div id="utilisateur">
    <p><strong>Date:</strong> <?php echo date("d/m/Y"); ?></p>
    <a href="/?p=chat.logout">Se déconnecter</a>
  </div>
 
  <div id="content">
        <h2>Utilisateurs connectés</h2>
        <ul id="users"></ul>

        <h2>Messages</h2>
        <ul id="messages"></ul>

        <form method="post">
        	<textarea rows="4" name="content"></textarea>
        	<br/>
        	<input type="submit" name="Envoyer">
        </form>
<!-- end Blocs-->  
  </div>

<!-- End bloc Container-->
</div>
</body>
</html>
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script type="text/javascript">

    refresh();

    setInterval(
        function () {
            refresh();
        }, 3000
    );

    function refresh() {
    	$.ajax(
        {
            type : "POST",
            dataType : "json",
            url : "/?p=chat.refresh",
            success : function(data) {
            	$("#messages").empty();
            	for (result in data) {
					$("#messages").append('<li><strong>' + data[result].user + '</strong> <i>[' + data[result].datetime + ']</i> :' + data[result].content + '</li>');
            	}
            }
        });

        $.ajax(
        {
            type : "POST",
            dataType : "json",
            url : "/?p=chat.checkConnection",
            success : function(data) {
            	$("#users").empty();
            	for (result in data) {
            		$("#users").append('<li>' + data[result] + '</li>');
            	}
            }
        });
    }
</script>